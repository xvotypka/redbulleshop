<?php

declare(strict_types=1);

namespace VotypkaMartinUser;

// komentář na jeden řádek


/**
 * komentář na
 * V
 * í
 * c
 * e
 *
 * Ř
 * á
 * d
 * k
 * ů
 */

/** Základy OOP v PHP
 * @author Martin Votýpka
 * @todo Programování v PHP
 */
// základy OOP
class UserMartinVotypka
{

    //privatni atributy
    private $aPrivate;
    private $bPrivate;

    //chranene atributy
    protected $cProtected;
    protected $dProtected;
    protected $eProtected;

    //verejne atributy
    public $firstPublic = "<h1>Vítám vás na této stránce.</h1>";
    public $secondPublic = true;
    public $thirdPublic = 'string';
    public $fourthPublic = 20041889;
    public $fifthPublic = 5.2;

    //konstruktor
    public function __construct()
    {
    }

    //private

    /** Private metoda aPrivate
     * @param int $aPrivate
     * @return int
     */
    public function setaPrivateMartinVotypka(int $aPrivate): int
    {
        return $aPrivate;
    }

    /** Private metoda bPrivate
     * @param string $bPrivate
     * @return string
     */
    public function setbPrivateMartinVotypka(string $bPrivate): string
    {
        return $bPrivate;
    }

    //public

    /** Public metoda getfirstPublicMartinVotypka
     * @return string
     */
    public function getfirstPublicMartinVotypka(): string
    {
        return $this->firstPublic;
    }

    /** Public metoda getsecondPublicMartinVotypka
     * @return bool
     */
    public function getsecondPublicMartinVotypka(): bool
    {
        return $this->secondPublic;
    }

    /** Public metoda getthirdPublicMartinVotypka
     * @return string
     */
    public function getthirdPublicMartinVotypka(): string
    {
        return $this->thirdPublic;
    }

    /** Public metoda getfourthPublicMartinVotypka
     * @return int
     */
    public function getfourthPublicMartinVotypka(): int
    {
        return $this->fourthPublic;
    }

    /** Public metoda getfifthPublicMartinVotypka
     * @return float
     */
    public function getfifthPublicMartinVotypka(): float
    {
        return $this->fifthPublic;
    }
}
?>